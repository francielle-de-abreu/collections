import { REQUEST } from "../actions/types";

const defaulState = { charactersList: [] };

function reducer(state = defaulState, action) {
  switch (action.type) {
    case REQUEST:
      return {
        charactersList: action.newRequest,
      };
    default:
      return state;
  }
}

export default reducer;
