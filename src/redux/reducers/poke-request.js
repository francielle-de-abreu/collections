import { REQUEST } from "../actions/types";

const defaulState = { charactersListPoke: [] };

function reducer(state = defaulState, action) {
  switch (action.type) {
    case REQUEST:
      return {
        charactersListPoke: action.newRequest,
      };
    default:
      return state;
  }
}

export default reducer;
