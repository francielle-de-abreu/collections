import { ADICIONAR } from "../actions/types";
import { REMOVER } from "../actions/types";
const defaulState = {
  characters:
    window.localStorage.getItem("collection") === null
      ? []
      : JSON.parse(window.localStorage.getItem("collection")),
};

function reducer(state = defaulState, action) {
  switch (action.type) {
    case REMOVER:
      window.localStorage.setItem(
        "collection",
        JSON.stringify([...action.selectCara])
      );
      return {
        characters: [...action.selectCara],
      };
    case ADICIONAR:
      window.localStorage.setItem(
        "collection",
        JSON.stringify([...state.characters, action.selectCara])
      );
      return {
        characters: [...state.characters, action.selectCara],
      };
    default:
      return state;
  }
}

export default reducer;
