import { HANDLE_TYPE } from "../actions/types";

const defaulState = { handleType: false };

function reducer(state = defaulState, action) {
  switch (action.type) {
    case HANDLE_TYPE:
      return {
        handleType: !state.handleType,
      };

    default:
      return state;
  }
}

export default reducer;
