import { combineReducers } from "redux";
import characters from "./characters";
import charactersList from "./rick-request";
import charactersListPoke from "./poke-request";
import handleType from "./handle-type";

export default combineReducers({
  characters,
  charactersList,
  charactersListPoke,
  handleType,
});
