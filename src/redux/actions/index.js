import { ADICIONAR } from "../actions/types";
import { REMOVER } from "../actions/types";
import { REQUEST } from "../actions/types";
import { HANDLE_TYPE } from "../actions/types";

export const remover = (selectCara) => ({
  type: REMOVER,
  selectCara,
});

export const adicionar = (selectCara) => ({
  type: ADICIONAR,
  selectCara,
});

export const request = (newRequest) => ({
  type: REQUEST,
  newRequest,
});

export const handleType = () => ({
  type: HANDLE_TYPE,
});
