import React from "react";
import { Link } from "react-router-dom";
import { MdCollectionsBookmark } from "react-icons/md";
import { BsFillPieChartFill } from "react-icons/bs";
import { CgGhostCharacter } from "react-icons/cg";
import NavStyle from "../../styled/nav";
const Routes = [
  {
    path: "/",
    name: "Collection",
    icon: <MdCollectionsBookmark />,
  },

  {
    path: "/characters",
    name: "Characters",
    icon: <CgGhostCharacter />,
  },
  {
    path: "/chart",
    name: "Chart",
    icon: <BsFillPieChartFill />,
  },
];

const Links = () => {
  return (
    <NavStyle>
      {Routes.map((item, key) => (
        <Link key={key} exact to={item.path}>
          {item.name}
          {item.icon}
        </Link>
      ))}
    </NavStyle>
  );
};

export default Links;
