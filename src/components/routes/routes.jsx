import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "../../pages/home";
import Chart from "../../pages/chart";
import Characters from "../../pages/characters";

const Router = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/characters">
        <Characters />
      </Route>
      <Route exact path="/chart">
        <Chart />
      </Route>
    </Switch>
  );
};

export default Router;
