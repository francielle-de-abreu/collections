import React from "react";
import { Card } from "antd";
import DivCharacters from "../styled/div-characters";
import Animation from "../styled/animation";

const GeneralCharacter = (props) => {
  const HandleIdImage = (url) => {
    const brokenUrl = url.split("/");
    const imagem = brokenUrl[brokenUrl.length - 2];
    return imagem;
  };

  return (
    <DivCharacters whileHover={{ scale: 1.2 }} whileTap={{ scale: 0.8 }}>
      {props.chacFilter !== undefined ? (
        props.chacFilter.map(({ name, url, species, image }, key) => (
          <Animation whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }}>
            <Card
              onClick={() => {
                props.handleCharacter({ name, url, species, image });
              }}
              key={key}
              hoverable
              style={{ width: 240 }}
              cover={
                <img
                  alt="image"
                  src={
                    species
                      ? image
                      : `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${HandleIdImage(
                          url
                        )}.png`
                  }
                />
              }
            >
              <Card.Meta title={name} />

              {species && <p>{species}</p>}
            </Card>
          </Animation>
        ))
      ) : (
        <DivCharacters>
          {props.charactersList &&
            props.charactersList.map(({ name, url, species, image }, key) => (
              <Animation whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }}>
                <Card
                  onClick={() => {
                    props.handleCharacter({ name, url, species, image });
                  }}
                  key={key}
                  hoverable
                  style={{ width: 240 }}
                  cover={
                    <img
                      alt="image"
                      src={
                        species
                          ? image
                          : `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${HandleIdImage(
                              url
                            )}.png`
                      }
                    />
                  }
                >
                  <Card.Meta title={name} />
                  {species && <p>{species}</p>}
                </Card>
              </Animation>
            ))}
        </DivCharacters>
      )}
    </DivCharacters>
  );
};

export default GeneralCharacter;
