import React, { useEffect, useState } from "react";
import axios from "axios";
import GeneralCharacter from "../general-character";
import TopBar from "../styled/top-bar";
import { useSelector, useDispatch } from "react-redux";
import { request } from "../../redux/actions";

const RickandMorty = (props) => {
  const dispatch = useDispatch();
  const listaRick = useSelector((state) => state.charactersList.charactersList);
  const [page, setPage] = useState(1);
  const urlRick = `https://rickandmortyapi.com/api/character/?page=${page}`;

  const addPagination = () => {
    if (page < 23) {
      setPage(page + 1);
    }
  };

  const subPagination = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  useEffect(() => {
    axios.get(urlRick).then((res) => {
      dispatch(request(res.data.results));
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  return (
    <div>
      <TopBar>
        <button onClick={subPagination}>Anterior</button>
        <button onClick={addPagination}>Próximo</button>
      </TopBar>
      <GeneralCharacter
        charactersList={listaRick}
        handleCharacter={props.handleCharacter}
      />
    </div>
  );
};

export default RickandMorty;
