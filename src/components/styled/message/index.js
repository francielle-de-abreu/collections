import styled from "styled-components";

const Message = styled.div`
  padding: 10px;
  font-size: 20px;
  display: flex;
  color: #1890ff;
  justify-content: space-around;
  background-color: #fae8dd;
`;
export default Message;
