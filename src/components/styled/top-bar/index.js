import styled from "styled-components";

const TopBar = styled.nav`
  width: 100%;
  font-weight: 800;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  color: #001529;
  font-size: 15px;
  padding-bottom: 30px;
  padding-top: 10px;
  background-color: #fae8dd;

  button {
    color: #fff;
    font-size: 1.1rem;
    background-color: #1a1a1a;
    transition: all 0.2s ease-in-out;
    display: inline-block;
    font-weight: 700;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid transparent;
    padding: 0.45rem 1.1rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: 0.4rem;
    margin-right: 20px;
  }

  button: hover {
    cursor: pointer;
    border-color: #1890ff;
    color: white;
    background-color: #1890ff;
  }

  select {
    width: 500px;
    border-radius: 5px;
  }

  @media (max-width: 425px) {
    button {
      font-size: 1rem;
      line-height: 0.8;
      border-radius: 0.4rem;
      margin-right: 1px;
    }

    button: hover {
      cursor: pointer;
      border-color: #1890ff;
      color: white;
      background-color: #1890ff;
    }

    select {
      width: 200px;
      border-radius: 5px;
    }
  }
`;
export default TopBar;
