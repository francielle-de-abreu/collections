import styled from "styled-components";

const NavStyle = styled.nav`
  width: 100%;
  display: flex;
  justify-content: center;
  color: #001529;
  font-size: 20px;
  background-color: #fae8dd;

  a {
    margin: 10px;
  }
`;
export default NavStyle;
