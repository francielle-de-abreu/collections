import styled from "styled-components";

const DivCharacters = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  background-color: #1a1a1a;
  padding: 10px;
  height: 100%;
  width: 100%;

  div {
    margin: 10px;
    text-align: center;
    border-radius: 20px;
    font-weight: 900;
  }

  section {
    color: #fff;
    font-size: 1.1rem;
    background-color: #1a1a1a;
    transition: all 0.2s ease-in-out;
    display: flex;
    font-weight: 700;
    text-align: center;
    align-items: center;
    justify-content: center;
    font-size: 1rem;
    height: 600px;
    width: 400px;
    padding-bottom: 150px;
  }
`;
export default DivCharacters;
