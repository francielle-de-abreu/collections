import React from "react";
import TopBar from "../styled/top-bar";
import { useSelector, useDispatch } from "react-redux";
import { handleType } from "../../redux/actions";

const ButtonSwitch = () => {
  const dispatch = useDispatch();
  const handleTypes = useSelector((state) => state.handleType.handleType);

  console.log(handleTypes);
  return (
    <TopBar>
      <button
        onClick={() => {
          dispatch(handleType(!handleTypes));
        }}
      >
        {handleTypes ? "Switch to Pokemon" : "Switch to Rick and Morty"}
      </button>
    </TopBar>
  );
};

export default ButtonSwitch;
