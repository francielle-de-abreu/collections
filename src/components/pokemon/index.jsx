import React, { useEffect, useState } from "react";
import axios from "axios";
import GeneralCharacter from "../general-character";
import TopBar from "../styled/top-bar";
import { useSelector, useDispatch } from "react-redux";
import { request } from "../../redux/actions";

const Pokemon = (props) => {
  const dispatch = useDispatch();
  const listaPokemon = useSelector(
    (state) => state.charactersListPoke.charactersListPoke
  );
  const [page, setPage] = useState(20);
  const urlPokemon = `https://pokeapi.co/api/v2/pokemon?offset=${page}&limit=10`;

  const addPagination = () => {
    if (page < 150) {
      setPage(page + 10);
    }
  };

  const subPagination = () => {
    if (page > 20) {
      setPage(page - 10);
    }
  };

  useEffect(() => {
    axios.get(urlPokemon).then((res) => {
      dispatch(request(res.data.results));
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  return (
    <div>
      <TopBar>
        <button onClick={subPagination}>Anterior</button>
        <button onClick={addPagination}>Próximo</button>
      </TopBar>
      <GeneralCharacter
        charactersList={listaPokemon}
        handleCharacter={props.handleCharacter}
      />
    </div>
  );
};

export default Pokemon;
