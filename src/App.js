import React from "react";
import Links from "./components/routes/links";
import Router from "./components/routes/routes";

const App = () => {
  return (
    <div className="App">
      <Links />
      <Router />
    </div>
  );
};

export default App;
