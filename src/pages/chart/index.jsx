import React from "react";
import { Pie } from "react-chartjs-2";
import DivCharacters from "../../components/styled/div-characters";
import { useSelector } from "react-redux";

const Chart = () => {
  const lista = useSelector((state) => state.characters.characters);

  const charactersData = {};

  const types = lista.map((car) => car.type);
  for (let i = 0; i < types.length; i++) {
    let count = types[i];
    if (charactersData[count] === undefined) {
      charactersData[count] = 1;
    } else {
      charactersData[count]++;
    }
  }

  console.log(charactersData);
  const data = {
    labels: Object.keys(charactersData),
    datasets: [
      {
        data: Object.values(charactersData),
        backgroundColor: ["#FF6384", "#36A2EB"],
        hoverBackgroundColor: ["#FF6384", "#36A2EB"],
      },
    ],
  };

  console.log(data);
  return (
    <DivCharacters>
      <section>
        {lista.length === 0 ? (
          "Add characters to have a chart"
        ) : (
          <Pie data={data} />
        )}
      </section>
    </DivCharacters>
  );
};

export default Chart;
