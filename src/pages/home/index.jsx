import React, { useState } from "react";
import GeneralCharacter from "../../components/general-character";
import { notification } from "antd";
import TopBar from "../../components/styled/top-bar";
import Message from "../../components/styled/message";
import DivCharacters from "../../components/styled/div-characters";
import { remover } from "../../redux/actions";
import { useSelector, useDispatch } from "react-redux";

const Home = () => {
  const lista = useSelector((state) => state.characters.characters);
  const dispatch = useDispatch();
  const [inputValue, getInput] = useState(undefined);
  const [chacFilter, getchacFilter] = useState(undefined);
  const [borderFeedback, getFeedback] = useState(false);
  const list = [];
  const remove = (character) => {
    if (lista) {
      lista.forEach((card) => {
        if (card.name !== character.name) {
          notification.info({
            key: character.name,
            message: "removido",
            description: "removido",
          });
          list.push(card);
        }
      });
    }

    dispatch(remover(list));
  };

  const handleFilter = () => {
    getFeedback(false);
    if (inputValue === "rickAndMorty") {
      getchacFilter(lista.filter((card) => card.species));
    } else {
      if (lista) {
        getchacFilter(lista.filter((card) => !card.species));
      }
    }
  };

  const handleClearFilter = () => {
    getchacFilter(undefined);
    getInput(undefined);
  };
  console.log(lista);
  return (
    <div>
      <TopBar>
        <TopBar>
          <select
            onChange={(e) => {
              getFeedback(true);
              getInput(e.target.value);
            }}
            value={inputValue}
          >
            <option value="pokemon">Pokemon</option>
            <option value="rickAndMorty"> Rick And Morty</option>
          </select>
          <button
            style={{ backgroundColor: borderFeedback && " #1890ff" }}
            onClick={handleFilter}
          >
            Filter
          </button>
        </TopBar>
        <button onClick={handleClearFilter}>All</button>
        <button
          onClick={() => {
            window.localStorage.removeItem("collection");
            dispatch(remover(list));
            notification.info({
              message: "Todos removidos",
              description: "removido",
            });
          }}
        >
          Clear
        </button>
      </TopBar>
      <Message>{!inputValue && "Click on the card to remove"}</Message>
      {lista.length === 0 ? (
        <DivCharacters>
          <section>Add your collection</section>
        </DivCharacters>
      ) : (
        <GeneralCharacter
          chacFilter={chacFilter}
          charactersList={lista}
          handleCharacter={remove}
        />
      )}
    </div>
  );
};

export default Home;
