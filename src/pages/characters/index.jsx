import React from "react";
import RickandMorty from "../../components/rick-and-morty";
import Pokemon from "../../components/pokemon";
import ButtonSwitch from "../../components/button-switch";
import { notification } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { adicionar } from "../../redux/actions";

const Characters = () => {
  const dispatch = useDispatch();
  const lista = useSelector((state) => state.characters.characters);
  const handleTypes = useSelector((state) => state.handleType.handleType);

  const handleCharacter = (selectCara) => {
    const validation = lista.some((person) => person.name === selectCara.name);

    if (validation) {
      notification.error({
        key: selectCara.name,
        message: "ja adicionado",
        description: "ja adicionado",
      });
    } else {
      notification.success({
        key: selectCara.name,
        message: "sucesso",
        description: "Adicionado",
      });
      if (selectCara.image) {
        selectCara.type = "RickAndMorty";
      } else {
        selectCara.type = "PokeMon";
      }

      dispatch(adicionar(selectCara));
    }
  };

  return (
    <div>
      <ButtonSwitch />
      <div>
        {handleTypes ? (
          <RickandMorty handleCharacter={handleCharacter} />
        ) : (
          <Pokemon handleCharacter={handleCharacter} />
        )}
      </div>
    </div>
  );
};
export default Characters;
